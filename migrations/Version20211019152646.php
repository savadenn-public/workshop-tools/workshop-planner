<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211019152646 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task ALTER duration_min TYPE INT');
        $this->addSql('ALTER TABLE task ALTER duration_min SET DEFAULT NULL');
        $this->addSql('ALTER TABLE task ALTER duration_max TYPE INT');
        $this->addSql('ALTER TABLE task ALTER duration_max SET DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task ALTER duration_min TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE task ALTER duration_min SET DEFAULT NULL');
        $this->addSql('ALTER TABLE task ALTER duration_max TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE task ALTER duration_max SET DEFAULT NULL');
    }
}
