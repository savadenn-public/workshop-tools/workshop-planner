<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add BusinessCase status support
 */
final class Version20220208093524 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add BusinessCase status support';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE business_case ADD archive_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE business_case ADD status VARCHAR(255) DEFAULT \'open\' NOT NULL');
        $this->addSql('COMMENT ON COLUMN business_case.archive_date IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE business_case DROP archive_date');
        $this->addSql('ALTER TABLE business_case DROP status');
    }
}
