<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Rename table `resource` in `workforce`
 */
final class Version20220105132215 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Rename table `resource` in `workforce`';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE resource RENAME TO workforce');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE workforce RENAME TO resource');
    }
}
