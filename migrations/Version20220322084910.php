<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Create table of resource PlanningSettings
 */
final class Version20220322084910 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create table of resource PlanningSettings';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE planning_settings (id UUID NOT NULL, name VARCHAR(255) NOT NULL, owner VARCHAR(255) NOT NULL, block_width INT NOT NULL, mode VARCHAR(255) NOT NULL, granularity DOUBLE PRECISION NOT NULL, days_number INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN planning_settings.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_settings.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE planning_settings_workforce (planning_settings_id UUID NOT NULL, workforce_id UUID NOT NULL, PRIMARY KEY(planning_settings_id, workforce_id))');
        $this->addSql('CREATE INDEX IDX_4C0FCD0314BF58D1 ON planning_settings_workforce (planning_settings_id)');
        $this->addSql('CREATE INDEX IDX_4C0FCD03A25BA942 ON planning_settings_workforce (workforce_id)');
        $this->addSql('COMMENT ON COLUMN planning_settings_workforce.planning_settings_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN planning_settings_workforce.workforce_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE planning_settings_workforce ADD CONSTRAINT FK_4C0FCD0314BF58D1 FOREIGN KEY (planning_settings_id) REFERENCES planning_settings (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE planning_settings_workforce ADD CONSTRAINT FK_4C0FCD03A25BA942 FOREIGN KEY (workforce_id) REFERENCES workforce (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE planning_settings_workforce DROP CONSTRAINT FK_4C0FCD0314BF58D1');
        $this->addSql('DROP TABLE planning_settings');
        $this->addSql('DROP TABLE planning_settings_workforce');
    }
}
