#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
DIR_NAME := $(shell basename ${PROJECT_PATH})
export DIR_NAME
PROJECT_NAME := workshop-api
export PROJECT_NAME
PROJECT_URL=workshop.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

# command name that are also directories
.PHONY:

#-----------------------------------------
# Allow passing arguments to make
#-----------------------------------------
SUPPORTED_COMMANDS := test.unit
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif

#-----------------------------------------
# Help commands
#-----------------------------------------
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_\-\0.0-9]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#-----------------------------------------
# Commands
#-----------------------------------------
clean: ## Cleans up environnement
	@mkdir -p ./dist/docs && rm -rf ./dist/docs/*
	@mkdir -p ${HOME}/.cache/yarn
	@docker-compose -f docker-compose.yml -f docker-compose.override.yml down --remove-orphans

clean.all: ## Kill containers and remove volumes
	@docker-compose -f docker-compose.yml -f docker-compose.override.yml down --remove-orphans --volumes

#-----------------------------------------
# Docker
#-----------------------------------------
prepare: ## Prepare to work with or without fullstack
	@docker network inspect workshop >/dev/null 2>&1 || \
         docker network create --driver bridge workshop
	@docker volume create fullstack_jwt || true
	@docker stop workshop-full-caddy-api-1 || true
	@docker stop workshop-full-php-api-1 || true

docker.pull: ## Retrieves latest docker images
	@docker-compose pull

docker.build: docker.pull ## Build all docker images
	@docker-compose build

docker.up: ## Starts all containers
	@docker-compose up -d
	@./scripts/updatePstormDatasource.sh
	@echo "See all services https://docapi-${PROJECT_URL}/#services"

dev: clean prepare docker.build docker.up ## Starts dev stack

#-----------------------------------------
# Production like environment
#-----------------------------------------
prod.build: ## Production: Only build
	@docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull
	@docker-compose -f docker-compose.yml -f docker-compose.prod.yml build

prod.up: ## Production: Only restart
	@docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --force-recreate
	@./scripts/updatePstormDatasource.sh
	@docker-compose run --rm php bin/console hautelook:fixtures:load -n

prod: clean prepare prod.build prod.up ## Production: Clean, Builds and starts
	@docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull
#-----------------------------------------
# Php
#-----------------------------------------
php.test.jwt:
	@docker-compose run -e XDEBUG_MODE=off --rm php bin/console lexik:jwt:generate-keypair --env=test --skip-if-exists

php.test: php.test.jwt ## Launch php unit tests
	@docker-compose run -e XDEBUG_MODE=coverage --rm php bin/phpunit --coverage-text

php.style: ## Check php code style compliance
	@docker-compose run -e XDEBUG_MODE=off --rm php sh -c 'vendor/bin/phpcs --config-set installed_paths vendor/escapestudios/symfony2-coding-standard && vendor/bin/phpcbf --standard=phpcs.xml.dist'

#-----------------------------------------
# Builds
#-----------------------------------------
build.doc: ## Generates docs in all format in ./public
	@mkdir -p ./dist/docs && rm -rf ./dist/docs/*
	@docker-compose run --rm -e "SPI_WATCH=false" -e "SPI_FORMAT=all" doc

test_db: ## Recreate test database from local migrations
	@docker-compose exec -e XDEBUG_MODE=off php bin/console doctrine:database:drop -f --env=test --if-exists
	@docker-compose exec -e XDEBUG_MODE=off php bin/console doctrine:database:create --env=test --if-not-exists
	@docker-compose exec -e XDEBUG_MODE=off php bin/console doctrine:migrations:migrate -n --env=test
	@docker-compose exec -e XDEBUG_MODE=off php bin/console hautelook:fixtures:load --env=test -n
