# Technical stack

This project is built with API platform. 

* [Read the official "Getting Started" guide](https://api-platform.com/docs/distribution).

## Database
The app database is based on [PostgreSQL](https://www.postgresql.org/) v13.

::: note
See service `database` in `docker-compose.yaml`.
:::

The database structure is shown in the following diagram:

![App database structure](database.svg){width=700px}

### Table `doctrine_migration_versions`
This table tracks executed Doctrine migrations.

### Table `user`
This table stores local users. The `guid` field comes from service [Authentication](#authentication) and is  the global identifier of the user across services. 

### Table `workforce`
Data table of entity `App\Entity\Workforce`.

### Table `team_user`
Data table of entity `App\Entity\TeamUser`.

### Table `task`
Data table of entity `App\Entity\Task`.

### Table `business_case`
Data table of entity `App\Entity\BusinessCase`.


## Authentication
Authentication is based on [Json Web Token](https://jwt.io/) for several reasons:

* No database table is needed
* Used across services
* Proven technology
* Lots of library available

The JWT is provided by service [Authentication](https://gitlab.com/savadenn-public/workshop-tools/authentication/).
