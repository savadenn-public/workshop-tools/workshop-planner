# Installation

Start containers:
```bash
make dev
```

And load fixtures:

```bash
docker-compose exec php bin/console hautelook:fixtures:load
```
