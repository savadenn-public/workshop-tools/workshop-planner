# Running unit tests
This project ships with unit tests **and** functional tests.

::: note
See [types of tests](https://symfony.com/doc/current/testing.html#types-of-tests) for further details.
:::

## Unit tests
To execute unit tests only, run the following command:

```bash
docker-compose exec php bin/phpunit --exclude-group=functional
```

## Functional tests
Functional tests need a database populated with [fixtures](https://github.com/theofidry/AliceBundle).  
They are located under `/fixtures` directory.

To init the database, run the following command:

```bash
make test_db
```

As long as API is secured by JWT, you have to generate key pair for test environment that will be used to generate valid JWTs:

```bash
docker-compose exec php bin/console lexik:jwt:generate-keypair --env=test --skip-if-exists
```

Finally, run the tests:

```bash
docker-compose exec php bin/phpunit
```

::: note
The fixtures will be automatically loaded & reset by functional tests.  
For further details, see [official documentation](https://github.com/theofidry/AliceBundle#database-testing) of Hautelook\\AliceBundle.
:::
