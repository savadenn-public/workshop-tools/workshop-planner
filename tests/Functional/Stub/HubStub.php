<?php

declare(strict_types=1);

namespace App\Tests\Functional\Stub;

use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Jwt\TokenFactoryInterface;
use Symfony\Component\Mercure\Jwt\TokenProviderInterface;
use Symfony\Component\Mercure\Update;

/**
 * Stub class to disable Mercure updates during tests
 */
class HubStub implements HubInterface
{
    public function __construct(private string $url, private string $publicUrl)
    {
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getPublicUrl(): string
    {
        return $this->publicUrl;
    }

    public function getProvider(): TokenProviderInterface
    {
    }

    public function getFactory(): ?TokenFactoryInterface
    {
    }

    public function publish(Update $update): string
    {
        return 'id';
    }
}
