<?php

declare(strict_types=1);

namespace App\Tests\PeriodValidator;

use App\PeriodValidator\PeriodValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

/**
 * Class test of PeriodValidator
 */
class PeriodValidatorTest extends TestCase
{
    private PeriodValidator $validator;

    protected function setUp(): void
    {
        $validator       = Validation::createValidator();
        $this->validator = new PeriodValidator($validator);
    }

    /**
     * Provide test data for testIsDateValid
     *
     * @return array[]
     */
    public function dateProvider(): array
    {
        return [
            'null'                     => [null, false],
            'empty string'             => ['', false],
            'string'                   => ['foo', false],
            'int'                      => [42, false],
            'float'                    => [3.14, false],
            'array'                    => [[], false],
            'object'                   => [new \stdClass(), false],
            'invalid date 1'           => ['01-01-2021T00:00:00Z', false],
            'invalid date 2'           => ['2021-02-31T00:00:00Z', false],
            'invalid date 3'           => [new \DateTime(), false],
            'valid date'               => ['2021-02-17T00:00:00Z', true],
            'valid date with timezone' => ['2021-02-17T00:00:00+00:01', true],
        ];
    }

    /**
     * Test method PeriodValidator::isDateValid() with several data
     *
     * @dataProvider dateProvider
     *
     * @param mixed $value    Value to validate
     * @param bool  $expected Expected result
     */
    public function testIsDateValid(mixed $value, bool $expected): void
    {
        $actual = $this->validator->isDateTimeValid($value);
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test method PeriodValidator::isPeriodValid() expecting failure
     */
    public function testIsPeriodInvalid(): void
    {
        $actual = $this->validator->isPeriodValid('2021-01-30T00:00:00Z', '2021-01-01T00:00:00Z');
        $this->assertFalse($actual);
    }

    /**
     * Test method PeriodValidator::isPeriodValid() expecting success
     */
    public function testIsPeriodValid(): void
    {
        $actual = $this->validator->isPeriodValid('2021-01-01T00:00:00Z', '2021-01-31T00:00:00Z');
        $this->assertTrue($actual);

        $actual = $this->validator->isPeriodValid('2021-01-01T00:00:00Z', '2021-01-01T23:00:00-00:01');
        $this->assertTrue($actual);
    }
}
