<?php

declare(strict_types=1);

namespace App\Tests\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\UserProvider;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class UserProviderTest
 */
class UserProviderTest extends TestCase
{
    /**
     * Test loadUserByIdentifierAndPayload() with an already existing user expecting success
     */
    public function testLoadExistingUserByIdentifierAndPayload(): void
    {
        $user       = new User('foo_guid');
        $manager    = $this->createMock(EntityManagerInterface::class);
        $repository = $this->createMock(UserRepository::class);

        $manager->expects($this->exactly(2))
            ->method('getRepository')
            ->with('App\Entity\User')
            ->willReturn($repository);

        $manager->expects($this->never())->method('persist');

        $repository->expects($this->exactly(2))
            ->method('findOneBy')
            ->with(['guid' => 'foo_guid'])
            ->willReturn($user);

        $provider = new UserProvider($manager);
        $provider->loadUserByIdentifierAndPayload('foo_guid', []);
        $provider->loadUserByIdentifier('foo_guid', []);
    }

    /**
     * Test loadUserByIdentifierAndPayload() with a new user expecting persistance in database
     */
    public function testLoadNewUserByIdentifierAndPayload(): void
    {
        $expectedUser = new User('foo_guid');
        $manager      = $this->createMock(EntityManagerInterface::class);
        $repository   = $this->createMock(UserRepository::class);

        $manager->expects($this->once())
            ->method('getRepository')
            ->with('App\Entity\User')
            ->willReturn($repository);

        $manager->expects($this->once())
            ->method('persist')
            ->with($expectedUser);

        $repository->expects($this->once())
            ->method('findOneBy')
            ->with(['guid' => 'foo_guid'])
            ->willReturn(null);

        $provider = new UserProvider($manager);
        $provider->loadUserByIdentifierAndPayload('foo_guid', []);
    }

    /**
     * Test supportClass
     */
    public function testSupportClass(): void
    {
        $manager  = $this->createMock(EntityManagerInterface::class);
        $provider = new UserProvider($manager);

        $this->assertTrue($provider->supportsClass(User::class));
        $this->assertFalse($provider->supportsClass(self::class));
    }

    /**
     * Test refresh user expecting exception if called
     */
    public function testRefreshUser(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('User should never be reloaded from the session in a stateless context');

        $manager  = $this->createMock(EntityManagerInterface::class);
        $provider = new UserProvider($manager);
        $provider->refreshUser(new User('foo_guid'));
    }
}
