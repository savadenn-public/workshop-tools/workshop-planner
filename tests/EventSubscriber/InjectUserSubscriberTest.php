<?php

declare(strict_types=1);

namespace App\Tests\EventSubscriber;

use App\Entity\PlanningSettings;
use App\Entity\User;
use App\EventSubscriber\InjectUserSubscriber;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Test user injection into DocumentRequest resource
 */
class InjectUserSubscriberTest extends TestCase
{
    private InjectUserSubscriber $subscriber;
    private Security $security;

    protected function setUp(): void
    {
        $this->security   = $this->createMock(Security::class);
        $this->subscriber = new InjectUserSubscriber($this->security);
    }

    public function testGetSubscribedEvents(): void
    {
        $expected = ['kernel.request' => ['injectUserId', 1]];
        $actual   = $this->subscriber->getSubscribedEvents();

        $this->assertEquals($expected, $actual);
    }

    public function supportsProvider(): array
    {
        return [
            [null, 'foo', 'bar', false],
            [new PlanningSettings(), 'GET', 'api_planning_settings_get_collection', false],
            [new PlanningSettings(), 'POST', 'api_planning_settings_post_collection', true],
        ];
    }

    /**
     * @dataProvider supportsProvider
     *
     * @param mixed  $data     Data after deserialization
     * @param string $method   HTTP method of the request
     * @param string $route    Name of the route
     * @param bool   $expected Expected result
     */
    public function testSupports(mixed $data, string $method, string $route, bool $expected): void
    {
        $kernel  = $this->createMock(HttpKernelInterface::class);
        $request = Request::create('', method: $method);
        $request->attributes->set('data', $data);
        $request->attributes->set('_route', $route);

        $event = new RequestEvent($kernel, $request, 1);

        $actual = $this->subscriber->supports($event);

        $this->assertEquals($expected, $actual);
    }

    public function testInjectUserId(): void
    {
        $data    = new PlanningSettings();
        $kernel  = $this->createMock(HttpKernelInterface::class);
        $request = new Request();
        $request->attributes->set('data', $data);
        $event = new RequestEvent($kernel, $request, 1);

        $subscriber = $this->getMockBuilder(InjectUserSubscriber::class)
            ->setConstructorArgs([$this->security])
            ->onlyMethods(['supports'])
            ->getMock();

        $subscriber->method('supports')->willReturn(true);
        $this->security
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(new User('foo-uuid'));

        $subscriber->injectUserId($event);

        $this->assertEquals('foo-uuid', $data->getOwner());
    }

    /**
     * Test subscriber with a real event dispatched
     */
    public function testInjectUserOnEvent(): void
    {
        $dispatcher = new EventDispatcher();
        $subscriber = new InjectUserSubscriber($this->security);

        $planningSettings = new PlanningSettings();

        $request = Request::create('/', 'POST');
        $request->attributes->set('data', $planningSettings);
        $request->attributes->set('_route', 'api_planning_settings_post_collection');

        $this->security
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(User::createFromPayload('foo-uuid', []));

        $kernel = $this->createMock(HttpKernelInterface::class);
        $event  = new RequestEvent($kernel, $request, HttpKernelInterface::MAIN_REQUEST);

        $dispatcher->addSubscriber($subscriber);
        $dispatcher->dispatch($event, 'kernel.request');

        $this->assertEquals('foo-uuid', $planningSettings->getOwner());
    }
}
