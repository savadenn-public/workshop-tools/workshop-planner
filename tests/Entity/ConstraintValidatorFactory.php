<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use Symfony\Component\Validator\ConstraintValidatorFactory as BaseConstraintValidatorFactory;

/**
 * Override BaseConstraintValidatorFactory to be able to manipulate registered validators
 */
class ConstraintValidatorFactory extends BaseConstraintValidatorFactory
{
    /**
     * Manually set a validator in the list
     *
     * @param string $id       Identifier of the validator
     * @param object $instance Instance of the validator
     *
     * @return $this
     */
    public function setValidator(string $id, object $instance): self
    {
        $this->validators[$id] = $instance;

        return $this;
    }
}
