<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\PlanningSettings;
use App\Entity\Team;
use PHPUnit\Framework\TestCase;

/**
 * Test class of PlanningSettings::class
 */
class PlanningSettingsTest extends TestCase
{
    /**
     * Test adding workforce
     */
    public function testAddWorkforce(): void
    {
        $workforce = new Team();
        $settings  = new PlanningSettings();

        $settings->addSelectedWorkforce($workforce);
        $this->assertCount(1, $settings->getSelectedWorkforces());

        // Try to add the exact same expecting workforce to be ignored
        $settings->addSelectedWorkforce($workforce);
        $this->assertCount(1, $settings->getSelectedWorkforces());
    }

    /**
     * Test removing workforce
     */
    public function testRemoveWorkforce(): void
    {
        $workforce = new Team();
        $settings  = new PlanningSettings();

        $settings->addSelectedWorkforce($workforce);
        $this->assertCount(1, $settings->getSelectedWorkforces());

        $settings->removeSelectedWorkforce($workforce);
        $this->assertCount(0, $settings->getSelectedWorkforces());
    }
}
