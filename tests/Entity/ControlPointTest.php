<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\ControlPoint;
use App\Entity\ControlPointValidation;
use App\Entity\Task;
use PHPUnit\Framework\TestCase;

/**
 */
class ControlPointTest extends TestCase
{
    /**
     * Test cloning ControlPoint expecting success
     */
    public function testClone(): void
    {
        $task         = new Task();
        $controlPoint = new ControlPoint($task);
        $controlPoint
            ->addControlPointValidation(new ControlPointValidation($controlPoint))
            ->setDescription('Foo description');

        $this->assertCount(1, $controlPoint->getControlPointValidations());

        $copy = clone $controlPoint;

        // Expect control point validations to be empty
        $this->assertCount(0, $copy->getControlPointValidations());

        // Expect description to be the same
        $this->assertEquals($controlPoint->getDescription(), $copy->getDescription());

        // Expect creation date to be reinitialized
        $this->assertNotEquals($controlPoint->getCreatedAt(), $copy->getCreatedAt());
    }
}
