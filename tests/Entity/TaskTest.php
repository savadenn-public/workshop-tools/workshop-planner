<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\ControlPoint;
use App\Entity\Task;
use PHPUnit\Framework\TestCase;

/**
 * Test class of entity Task
 */
class TaskTest extends TestCase
{
    /**
     * Test method getEstimatedEndMin()
     */
    public function testUpdateEndDates(): void
    {
        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-01-01 00:00:00');
        $task = new Task();
        $task->updateEstimatedEnds();

        // StartDate & estimatedEnds are null: expect null result
        $this->assertNull($task->getEstimatedEndMin());
        $this->assertNull($task->getEstimatedEndMax());

        // Set startDate but leave estimatedEnds null: expect null result
        $task->setStartDate($startDate);
        $task->updateEstimatedEnds();
        $this->assertNull($task->getEstimatedEndMin());
        $this->assertNull($task->getEstimatedEndMax());

        // Set estimatedEnds: expect result
        $task->setDurationMin(10*60); // 10h
        $task->setDurationMax(20*60); // 20h
        $task->updateEstimatedEnds();

        $actualMin = $task->getEstimatedEndMin();
        $expectedMin = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-01-01 10:00:00');
        $this->assertEquals($expectedMin, $actualMin);

        $actualMax = $task->getEstimatedEndMax();
        $expectedMax = \DateTime::createFromFormat('Y-m-d H:i:s', '2021-01-01 20:00:00');
        $this->assertEquals($expectedMax, $actualMax);
    }

    /**
     * Test clone behavior
     */
    public function testClone(): void
    {
        $task = new Task();
        $task
            ->setStartDate(new \DateTime('-1 day'))
            ->setEndDate(new \DateTimeImmutable())
            ->addControlPoint(new ControlPoint($task));

        $copy = clone $task;

        // Expect dates & ID to be reset
        $this->assertNull($copy->getStartDate());
        $this->assertNull($copy->getEndDate());
        $this->assertNull($copy->getId());
        $this->assertNotEquals($task->getCreatedAt(), $copy->getCreatedAt());

        // Expect control points count to be the same...
        $this->assertEquals($task->getControlPoints()->count(), $copy->getControlPoints()->count());

        // ...but to be a copy too
        $this->assertNotEquals($task->getControlPoints(), $copy->getControlPoints());

        /** @var ControlPoint $cp1 */
        $cp1 = $task->getControlPoints()->first();
        /** @var ControlPoint $cp2 */
        $cp2 = $copy->getControlPoints()->first();

        // Deep compare control point
        $this->assertEquals($cp1->getDescription(), $cp2->getDescription());
        $this->assertNotEquals($cp1->getCreatedAt(), $cp2->getCreatedAt());
        $this->assertEquals($copy, $cp2->getTask());
    }
}
