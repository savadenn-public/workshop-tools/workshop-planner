<?php

declare(strict_types=1);

namespace App\Tests\BusinessCaseStatus;

use App\BusinessCaseStatus\Status;
use App\BusinessCaseStatus\StatusCalculator;
use App\Entity\BusinessCase;
use PHPUnit\Framework\TestCase;

/**
 * Test class of StatusCalculator
 */
class StatusCalculatorTest extends TestCase
{
    /**
     * Test method getStatus()
     */
    public function testGetStatus(): void
    {
        $model      = new BusinessCase();
        $calculator = new StatusCalculator();

        $actual   = $calculator->getStatus($model);
        $expected = Status::Open;
        $this->assertEquals($expected, $actual);

        $model->setArchiveDate(new \DateTimeImmutable());

        $actual   = $calculator->getStatus($model);
        $expected = Status::Archived;
        $this->assertEquals($expected, $actual);
    }
}
