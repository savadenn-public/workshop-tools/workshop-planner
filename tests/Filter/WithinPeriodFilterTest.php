<?php

declare(strict_types=1);

namespace App\Tests\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use App\Entity\Task;
use App\Filter\WithinPeriodFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @group functional
 */
class WithinPeriodFilterTest extends KernelTestCase
{
    private QueryBuilder $queryBuilder;
    private WithinPeriodFilter $filter;

    protected function setUp(): void
    {
        static::bootKernel();
        $container = static::getContainer();

        $em = $container->get(EntityManagerInterface::class);
        /** @var QueryBuilder $queryBuilder */
        $this->queryBuilder = $em->getRepository(Task::class)->createQueryBuilder('o');
        $this->filter       = $container->get(WithinPeriodFilter::class);
    }

    /**
     * Test behavior of the filter
     */
    public function testApply(): void
    {
        $this->queryBuilder
            ->where('o.title = :title')
            ->setParameter('title', 'Foo');

        $context = [
            'filters' => [
                'period' => [
                    'from' => '2021-01-01T23:00:00Z',
                    'to'   => '2021-01-10T22:59:59+01:00',
                ],
            ],
        ];

        $this->filter->apply($this->queryBuilder, new QueryNameGenerator(), Task::class, 'get', $context);

        $actual   = $this->queryBuilder->getDQL();
        $expected = 'SELECT o FROM App\Entity\Task o WHERE o.title = :title AND ((o.startDate BETWEEN :from AND :to) OR (o.startDate < :from AND (o.endDate > :from OR o.estimatedEndMax > :from)))';

        $this->assertEquals($expected, $actual);

        $actual   = $this->queryBuilder->getParameters()->toArray();
        $expected = [
            new Parameter('title', 'Foo'),
            new Parameter('from', new \DateTime('2021-01-01T23:00:00Z')),
            new Parameter('to', new \DateTime('2021-01-10T22:59:59+01:00')),
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test filter expecting to not be applied
     */
    public function testDoesNotApply(): void
    {
        $context = ['filters' => ['anything' => 'foo']];
        $this->filter->apply($this->queryBuilder, new QueryNameGenerator(), 'Anything but Task', 'post', $context);
        $actual = $this->queryBuilder->getDQL();

        $this->assertEquals('SELECT o FROM App\Entity\Task o', $actual);
    }

    /**
     * Test params are not valid
     */
    public function testApplyInvalidParamFilter(): void
    {
        $this->expectException(BadRequestException::class);

        $context = [
            'filters' => [
                'period' => [
                    'from' => '2021-01-01T23:00:00Z',
                ],
            ],
        ];
        $this->filter->apply($this->queryBuilder, new QueryNameGenerator(), Task::class, 'get', $context);
    }
}
