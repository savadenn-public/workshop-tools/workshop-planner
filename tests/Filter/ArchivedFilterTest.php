<?php

declare(strict_types=1);

namespace App\Tests\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use App\Entity\Task;
use App\Filter\ArchivedFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @group functional
 */
class ArchivedFilterTest extends KernelTestCase
{
    private QueryBuilder $queryBuilder;
    private ArchivedFilter $filter;

    protected function setUp(): void
    {
        static::bootKernel();
        $container = static::getContainer();

        $em = $container->get(EntityManagerInterface::class);
        /** @var QueryBuilder $queryBuilder */
        $this->queryBuilder = $em->getRepository(Task::class)->createQueryBuilder('o');
        $this->filter       = $container->get(ArchivedFilter::class);
    }

    /**
     * Test filtering tasks of non-archived business cases
     */
    public function testFilterNonArchived(): void
    {
        $this->queryBuilder
            ->where('o.title = :title')
            ->setParameter('title', 'Foo');

        $context = [
            'filters' => [
                'archived' => false,
            ],
        ];

        $this->filter->apply($this->queryBuilder, new QueryNameGenerator(), Task::class, 'get', $context);

        $actual   = $this->queryBuilder->getDQL();
        $expected = 'SELECT o FROM App\Entity\Task o INNER JOIN o.businessCase bc WITH bc.status <> :status WHERE o.title = :title';

        $this->assertEquals($expected, $actual);

        $actual   = $this->queryBuilder->getParameters()->toArray();
        $expected = [
            new Parameter('title', 'Foo'),
            new Parameter('status', 'archived'),
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test filtering tasks of archived business cases
     */
    public function testFilterArchived(): void
    {
        $this->queryBuilder
            ->where('o.title = :title')
            ->setParameter('title', 'Foo');

        $context = [
            'filters' => [
                'archived' => true,
            ],
        ];

        $this->filter->apply($this->queryBuilder, new QueryNameGenerator(), Task::class, 'get', $context);

        $actual   = $this->queryBuilder->getDQL();
        $expected = 'SELECT o FROM App\Entity\Task o INNER JOIN o.businessCase bc WITH bc.status = :status WHERE o.title = :title';

        $this->assertEquals($expected, $actual);

        $actual   = $this->queryBuilder->getParameters()->toArray();
        $expected = [
            new Parameter('title', 'Foo'),
            new Parameter('status', 'archived'),
        ];
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test filter expecting to not be applied
     */
    public function testDoesNotApply(): void
    {
        $context = ['filters' => ['anything' => 'foo']];
        $this->filter->apply($this->queryBuilder, new QueryNameGenerator(), 'Anything but Task', 'post', $context);
        $actual = $this->queryBuilder->getDQL();

        $this->assertEquals('SELECT o FROM App\Entity\Task o', $actual);
    }

    /**
     * Test invalid param expecting bad request
     */
    public function testApplyInvalidParamFilter(): void
    {
        $this->expectException(BadRequestException::class);
        $this->expectExceptionMessage('Invalid value of type string.');

        $context = [
            'filters' => [
                'archived' => 'not a boolean',
            ],
        ];
        $this->filter->apply($this->queryBuilder, new QueryNameGenerator(), Task::class, 'get', $context);
    }
}
