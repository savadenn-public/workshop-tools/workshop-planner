<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\CloneBusinessCaseController;
use App\Entity\BusinessCase;
use App\Repository\BusinessCaseRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Test class of CloneBusinessCaseController
 */
class CloneBusinessCaseControllerTest extends TestCase
{
    /**
     * Test cloning not-existing business case expecting exception
     */
    public function testCloneResourceNotFound(): void
    {
        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('Resource foo not found');

        $repository = $this->createMock(BusinessCaseRepository::class);
        $repository->expects($this->once())
            ->method('findDeep')
            ->with('foo')
            ->willReturn(null);

        $controller = new CloneBusinessCaseController($repository);

        $controller('foo', new BusinessCase());
    }
}
