<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\BusinessCase;
use App\Entity\Machine;
use App\Repository\BusinessCaseRepository;
use App\Repository\MachineRepository;
use App\Repository\TaskRepository;
use Doctrine\Common\Collections\Criteria;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * Class TaskTest
 *
 * @group functional
 */
class TaskTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        $client = $this->createAuthenticatedClient();

        $response = $client->request('GET', '/tasks');

        $this->assertResponseIsSuccessful();
        $result = $response->toArray();

        $this->assertEquals(1000, $result['hydra:totalItems']);
    }

    /**
     * Test filtering tasks by assignee expecting success
     */
    public function testGetFilteredCollection(): void
    {
        /** @var Machine $machine */
        $machine = static::getContainer()->get(MachineRepository::class)->findOneBy(['name' => 'Machine having a unique task']);
        $client  = $this->createAuthenticatedClient();

        $response = $client->request('GET', '/tasks', [
            'query' => ['assignedTo' => $machine->getId()],
        ]);

        $this->assertResponseIsSuccessful();
        $result = $response->toArray();

        $this->assertEquals(1, $result['hydra:totalItems']);
    }

    public function testCreate(): void
    {
        $client = $this->createAuthenticatedClient();
        /** @var BusinessCase $businessCase */
        $businessCase = static::getContainer()->get(BusinessCaseRepository::class)->findBy([], limit: 1)[0];
        /** @var Machine $assignee */
        $assignee = static::getContainer()->get(MachineRepository::class)->findBy([], limit: 1)[0];

        $client->request('POST', '/tasks', [
            'headers' => ['content-type' => 'application/json'],
            'json'    => [
                'title'        => 'Make some coffee',
                'description'  => 'My awesome task',
                'businessCase' => '/business_cases/'.$businessCase->getId(),
                'assignedTo'   => '/machines/'.$assignee->getId(),
                'durationMin'  => 10,
                'durationMax'  => 15,
                'startDate'    => '2021-01-01',
                'endDate'      => '2021-01-10',
            ],
        ]);

        $this->assertResponseIsSuccessful();
    }

    public function testCreateFailed(): void
    {
        $client = $this->createAuthenticatedClient();

        // Items not found
        $response = $client->request('POST', '/tasks', [
            'headers' => ['content-type' => 'application/json'],
            'json'    => [
                'businessCase' => '/business_cases/0322b1fa-0000-0000-0000-ff3db220c701',
                'assignedTo'   => '/machines/0322b1fa-0000-0000-0000-ff3db220c701',
            ],
        ]);

        $result = $response->toArray(false);
        $this->assertResponseStatusCodeSame(400);
        $this->assertEquals('Item not found for "/business_cases/0322b1fa-0000-0000-0000-ff3db220c701".', $result['hydra:description']);

        // Invalid data
        $client->request('POST', '/tasks', [
            'headers' => ['content-type' => 'application/json'],
            'json'    => [
                'title'       => '',
                'durationMin' => 10,
                'durationMax' => 5,
                'startDate'   => '2021-01-01',
                'endDate'     => '2019-01-01',
            ],
        ]);

        $this->assertResponseStatusCodeSame(422);
    }

    /**
     * Test patching task start date expecting failure
     */
    public function testResetStartDateFail(): void
    {
        $client = $this->createAuthenticatedClient();
        $task   = static::getContainer()->get(TaskRepository::class)->findOneBy([]);

        $client->request('PATCH', '/tasks/'.$task->getId(), [
            'headers' => ['content-type' => 'application/merge-patch+json'],
            'json'    => [
                'startDate' => null,
                'endDate'   => '2022-01-01',
            ],
        ]);

        $this->assertResponseStatusCodeSame(422);
    }
}
