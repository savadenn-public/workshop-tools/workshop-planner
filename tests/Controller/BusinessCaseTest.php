<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\BusinessCase;
use App\Repository\BusinessCaseRepository;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * Class BusinessCaseTest
 *
 * @group functional
 */
class BusinessCaseTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    public function testGetCollection(): void
    {
        $response = $this->createAuthenticatedClient()->request('GET', '/business_cases');

        $this->assertResponseIsSuccessful();
        $this->assertEquals(10, $response->toArray()['hydra:totalItems']);
    }

    public function testCreateBusinessCase(): void
    {
        $response = $this->createAuthenticatedClient()->request('POST', '/business_cases', [
            'headers' => [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'json'    => [
                'code'    => 'BC0228',
                'name'    => 'Rafale alu 7021',
                'dueDate' => (new \DateTime('+3 months'))->format('Y-m-d H:i:s'),
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $response = $response->toArray();

        $this->assertArraySubset([
            'code'   => 'BC0228',
            'name'   => 'Rafale alu 7021',
            'status' => 'open',
        ], $response);
    }

    /**
     * Smoke test clone-business-case endpoint
     */
    public function testClone(): void
    {
        /** @var BusinessCase $businessCase */
        $businessCase = static::getContainer()->get(BusinessCaseRepository::class)->findOneBy([]);
        $client       = $this->createAuthenticatedClient();

        $client->request('POST', sprintf('/business_cases/%s/clone', $businessCase->getId()), [
            'json' => [
                'code' => 'H666',
                'name' => 'Hell case',
            ],
        ]);

        $this->assertResponseStatusCodeSame(201);
    }

    /**
     * Test creating a business case with an archive date
     */
    public function testCreateArchived(): void
    {
        $client   = $this->createAuthenticatedClient();
        $response = $client->request('POST', '/business_cases', [
            'json' => [
                'code'        => 'H2G2',
                'name'        => 'The Hitchhiker’s Guide',
                'archiveDate' => '2022-01-01 20:02:14',
            ],
        ]);

        $this->assertResponseStatusCodeSame(201);
        $actual   = $response->toArray()['status'];
        $expected = 'archived';

        $this->assertEquals($expected, $actual);
    }

    /**
     * Test archiving an open BusinessCase
     */
    public function testArchive(): void
    {
        /** @var BusinessCase $businessCase */
        $businessCase = static::getContainer()->get(BusinessCaseRepository::class)->findOneBy([]);
        $client       = $this->createAuthenticatedClient();

        $response = $client->request('PATCH', sprintf('/business_cases/%s', $businessCase->getId()), [
            'headers' => ['Content-Type' => 'application/merge-patch+json'],
            'json'    => [
                'archiveDate' => '2022-01-01 10:42:45',
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals('archived', $response->toArray()['status']);
    }
}
