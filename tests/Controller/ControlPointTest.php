<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * @group functional
 */
class ControlPointTest extends AbstractApiTestCase
{
    use RefreshDatabaseTrait;

    /**
     * Smoke test get collection filtered by task
     */
    public function testGetCollection(): void
    {
        /** @var Task $task */
        $task = static::getContainer()->get(TaskRepository::class)->findOneBy(['title' => 'A unique task assigned to a unique machine']);

        $response = $this->createAuthenticatedClient()->request('GET', '/control_points', [
            'query' => ['task' => $task->getId()],
        ]);

        $this->assertResponseIsSuccessful();

        $result = $response->toArray();

        $this->assertCount(5, $result['hydra:member']);
    }

    /**
     * Smoke test creation
     */
    public function testCreate(): void
    {
        /** @var Task $task */
        $task = static::getContainer()->get(TaskRepository::class)->findOneBy(['title' => 'A unique task assigned to a unique machine']);

        $this->createAuthenticatedClient()->request('POST', '/control_points', [
            'json' => [
                'task'        => '/tasks/'.$task->getId(),
                'description' => 'Check something',
            ],
        ]);

        $this->assertResponseIsSuccessful();
    }
}
