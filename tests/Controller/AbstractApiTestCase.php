<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

/**
 * Class AbstractApiTestCase
 */
abstract class AbstractApiTestCase extends ApiTestCase
{
    /**
     * Create a http client with a valid Authorization header
     *
     * @param string $guid See fixtures/users.yaml for matching guids
     *
     * @return Client
     */
    protected function createAuthenticatedClient(string $guid = '0322b1fa-d41b-4a86-9d66-ff3db220c701'): Client
    {
        $manager = static::getContainer()->get(JWTTokenManagerInterface::class);
        $user    = User::createFromPayload($guid, []);
        $jwt     = $manager->create($user);

        return static::createClient([], ['headers' => ['authorization' => 'Bearer '.$jwt]]);
    }
}
