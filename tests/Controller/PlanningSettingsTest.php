<?php

declare(strict_types=1);

namespace App\Tests\Controller;

/**
 * @group functional
 */
class PlanningSettingsTest extends AbstractApiTestCase
{
    /**
     * Test resource creation expecting success
     */
    public function testCreate(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('POST', '/planning_settings', [
            'json' => [
                'name'               => 'test',
                'blockWidth'         => 4,
                'mode'               => 'by_workforce',
                'granularity'        => 6,
                'daysNumber'         => 7,
                'selectedWorkforces' => [],
            ],
        ]);

        $this->assertResponseIsSuccessful();
    }
}
