<?php

declare(strict_types=1);

namespace App\Tests\EventListener\Doctrine;

use App\BusinessCaseStatus\Status;
use App\BusinessCaseStatus\StatusCalculator;
use App\Entity\BusinessCase;
use App\EventListener\Doctrine\BusinessCaseStatusUpdater;
use PHPUnit\Framework\TestCase;

/**
 * Test class of BusinessCaseStatusUpdater
 */
class BusinessCaseStatusUpdaterTest extends TestCase
{
    /**
     * Test method updateStatus()
     */
    public function testUpdateStatus(): void
    {
        $calculator = $this->createMock(StatusCalculator::class);
        $model      = new BusinessCase();
        $updater    = new BusinessCaseStatusUpdater($calculator);

        // Expect calculator to be called
        $calculator->expects($this->once())
            ->method('getStatus')
            ->willReturn(Status::Archived);

        $updater->updateStatus($model);

        $this->assertEquals(Status::Archived, $model->getStatus());
    }
}
