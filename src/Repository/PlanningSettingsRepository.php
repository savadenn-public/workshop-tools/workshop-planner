<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PlanningSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlanningSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanningSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanningSettings[]    findAll()
 * @method PlanningSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningSettingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlanningSettings::class);
    }
}
