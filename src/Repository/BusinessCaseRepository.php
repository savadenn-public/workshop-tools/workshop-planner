<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\BusinessCase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusinessCase|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessCase|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessCase[]    findAll()
 * @method BusinessCase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessCaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessCase::class);
    }

    /**
     * Get a BusinessCase with all its sub-trees joined in a single query
     *
     * @param string $id
     *
     * @return BusinessCase|null
     */
    public function findDeep(string $id): ?BusinessCase
    {
        return $this->createQueryBuilder('b')
            ->leftJoin('b.tasks', 't')
            ->leftJoin('t.assignedTo', 'r')
            ->addSelect('t')
            ->addSelect('r')
            ->where('b.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
