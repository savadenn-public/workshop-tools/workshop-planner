<?php

declare(strict_types=1);

namespace App\BusinessCaseStatus;

use App\Entity\BusinessCase;

/**
 * Interface StatusCalculatorInterface
 */
interface StatusCalculatorInterface
{
    /**
     * Compute status of given business case
     *
     * @param BusinessCase $model
     *
     * @return Status
     */
    public function getStatus(BusinessCase $model): Status;
}
