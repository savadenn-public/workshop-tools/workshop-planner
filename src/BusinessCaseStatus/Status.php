<?php
// phpcs:ignoreFile
declare(strict_types=1);

namespace App\BusinessCaseStatus;

/**
 * Business case statuses
 */
enum Status: string
{
    /**
     * Default status of a business case
     */
    case Open = 'open';

    /**
     * Business case is archived
     */
    case Archived = 'archived';
}
