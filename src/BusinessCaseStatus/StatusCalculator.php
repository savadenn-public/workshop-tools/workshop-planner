<?php

declare(strict_types=1);

namespace App\BusinessCaseStatus;

use App\Entity\BusinessCase;

/**
 * Class StatusCalculator
 */
class StatusCalculator implements StatusCalculatorInterface
{
    public function getStatus(BusinessCase $model): Status
    {
        $status = Status::Open;

        if ($model->getArchiveDate()) {
            $status = Status::Archived;
        }

        return $status;
    }
}
