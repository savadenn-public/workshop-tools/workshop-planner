<?php

declare(strict_types=1);

namespace App\PeriodValidator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Validator for period
 */
class PeriodValidator
{
    public function __construct(private ValidatorInterface $validator)
    {
    }

    /**
     * Return whether given period is valid
     * A valid period is composed of two valid dates with $to >= $from
     *
     * @param mixed $from Start of period
     * @param mixed $to   End of period
     *
     * @return bool
     */
    public function isPeriodValid(mixed $from, mixed $to): bool
    {
        return $this->isDateTimeValid($from) && $this->isDateTimeValid($to) && strtotime($from) <= strtotime($to);
    }

    /**
     * Return whether given value is a valid datetime
     *
     * @param mixed $date The value to validate
     *
     * @return bool
     */
    public function isDateTimeValid(mixed $date): bool
    {
        $violations = $this->validator->validate($date, [
            new Assert\NotBlank(),
            new Assert\DateTime(\DateTime::ATOM),
        ]);

        return $violations->count() === 0;
    }
}
