<?php

declare(strict_types=1);

namespace App\Dto;

use App\Entity\ControlPoint;

/**
 * DTO class for ControlPointValidation input
 */
class ControlPointValidationInput
{
    public ControlPoint $controlPoint;
}
