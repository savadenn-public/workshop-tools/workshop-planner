<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\PlanningSettings;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

/**
 * Service in charge of injecting current user in a DocumentRequest being created
 */
class InjectUserSubscriber implements EventSubscriberInterface
{
    public function __construct(private Security $security)
    {
    }

    public function injectUserId(RequestEvent $event)
    {
        /** @var PlanningSettings $object */
        $object = $event->getRequest()->attributes->get('data');

        if ($this->supports($event)) {
            $object->setOwner($this->security->getUser()->getUserIdentifier());
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['injectUserId', EventPriorities::POST_DESERIALIZE],
        ];
    }

    public function supports(RequestEvent $event): bool
    {
        // Get deserialized data from request (see DeserializeListener::class)
        $object    = $event->getRequest()->attributes->get('data');
        $routeName = $event->getRequest()->attributes->get('_route');

        return $object instanceof PlanningSettings
            && $event->getRequest()->getMethod() === Request::METHOD_POST
            && 'api_planning_settings_post_collection' === $routeName;
    }
}
