<?php

declare(strict_types=1);

namespace App\EventListener\Doctrine;

use App\BusinessCaseStatus\StatusCalculatorInterface;
use App\Entity\BusinessCase;

/**
 * In charge of re-compute BusinessCase status before inserts/updates
 */
class BusinessCaseStatusUpdater
{
    public function __construct(private StatusCalculatorInterface $calculator)
    {
    }

    public function updateStatus(BusinessCase $case): void
    {
        $case->setStatus($this->calculator->getStatus($case));
    }
}
