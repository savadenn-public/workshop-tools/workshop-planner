<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\PayloadAwareUserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserProvider
 */
class UserProvider implements PayloadAwareUserProviderInterface
{
    /**
     * UserProvider constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(private EntityManagerInterface $manager)
    {
    }

    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function loadUserByUsernameAndPayload(string $username, array $payload): User
    {
        return $this->loadUserByIdentifierAndPayload($username, $payload);
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        throw new \Exception('User should never be reloaded from the session in a stateless context');
    }

    public function supportsClass(string $class): bool
    {
        return User::class === $class;
    }

    /**
     * @inheritDoc
     *
     * @codeCoverageIgnore
     */
    public function loadUserByUsername(string $username, array $payload = []): User
    {
        return $this->loadUserByIdentifierAndPayload($username, $payload);
    }

    public function loadUserByIdentifierAndPayload(string $identifier, array $payload): User
    {
        $user = $this->manager
            ->getRepository(User::class)
            ->findOneBy(['guid' => $identifier]);

        // If user does exist yet, create it from payload
        if (!$user) {
            $user = User::createFromPayload($identifier, $payload);

            $this->manager->persist($user);
            $this->manager->flush();
        }

        return $user;
    }

    public function loadUserByIdentifier(string $identifier, array $payload = []): User
    {
        return $this->loadUserByIdentifierAndPayload($identifier, $payload);
    }
}
