<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\ControlPointValidation;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Voter for ControlPointValidation
 */
class ControlPointValidationVoter extends Voter
{
    protected function supports(string $attribute, $subject): bool
    {
        return 'DELETE' === $attribute && $subject instanceof ControlPointValidation;
    }

    /**
     * @param string                 $attribute
     * @param ControlPointValidation $subject
     * @param TokenInterface         $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        return $subject->getVerifier() === $user->getGuid();
    }
}
