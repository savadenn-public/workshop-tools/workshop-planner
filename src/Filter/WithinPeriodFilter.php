<?php

declare(strict_types=1);

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Task;
use App\PeriodValidator\PeriodValidator;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

/**
 * Specific filter for Task resources: fetch all tasks being within a given period
 */
class WithinPeriodFilter extends AbstractContextAwareFilter
{
    const FILTER = 'period';

    public function __construct(private PeriodValidator $validator, ManagerRegistry $managerRegistry, ?RequestStack $requestStack = null, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null)
    {
        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);
    }

    public function getDescription(string $resourceClass): array
    {
        $description = [];

        $description[self::FILTER.'[from]'] = [
            'property' => 'from',
            'type'     => Type::BUILTIN_TYPE_STRING,
            'required' => false,
            'exemple'  => '2021-01-01T23:00:00Z',
        ];

        $description[self::FILTER.'[to]'] = [
            'property' => 'to',
            'type'     => Type::BUILTIN_TYPE_STRING,
            'required' => false,
            'description' => 'bite',
            'exemple'  => '2021-01-01T23:00:00Z',
        ];

        return $description;
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null): void
    {
        if (self::FILTER !== $property || 'get' !== $operationName || Task::class !== $resourceClass) {
            return;
        }

        $from = $value['from'] ?? null;
        $to   = $value['to'] ?? null;

        if (!$from || !$to || !$this->validator->isPeriodValid($from, $to)) {
            throw new BadRequestException("Both period[from] and period[to] should be valid ISO8601 Datetime without milliseconds");
        }

        $periodQuery = $queryBuilder->expr()->orX();
        $periodQuery
            ->add('o.startDate BETWEEN :from AND :to')
            ->add('o.startDate < :from AND (o.endDate > :from OR o.estimatedEndMax > :from)');

        $queryBuilder
            ->andwhere($periodQuery)
            ->setParameter('from', new \DateTime($from))
            ->setParameter('to', new \DateTime($to));
    }
}
