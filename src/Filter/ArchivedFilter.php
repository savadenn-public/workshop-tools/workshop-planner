<?php

declare(strict_types=1);

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\BusinessCaseStatus\Status;
use App\Entity\Task;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\PropertyInfo\Type;

/**
 * Allow filtering task by their business case status (archived or not)
 */
final class ArchivedFilter extends AbstractContextAwareFilter
{
    public function getDescription(string $resourceClass): array
    {
        return [
            Status::Archived->value => [
                'property' => Status::Archived->value,
                'type'     => Type::BUILTIN_TYPE_BOOL,
                'required' => false,
                'exemple'  => true,
            ],
        ];
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if (Status::Archived->value !== $property || 'get' !== $operationName || Task::class !== $resourceClass) {
            return;
        }

        $condition = $this->normalizeValue($value) ? 'bc.status = :status' : 'bc.status <> :status';

        $queryBuilder
            ->innerJoin('o.businessCase', 'bc', Join::WITH, $condition)
            ->setParameter('status', Status::Archived->value);
    }

    /**
     * Cast given value as boolean value
     *
     * @param mixed $value
     *
     * @return bool
     */
    private function normalizeValue(mixed $value): bool
    {
        if (\in_array($value, [true, 'true', '1', '', null], true)) {
            return true;
        }

        if (\in_array($value, [false, 'false', '0'], true)) {
            return false;
        }

        throw new BadRequestException(sprintf('Invalid value of type %s.', gettype($value)));
    }
}
