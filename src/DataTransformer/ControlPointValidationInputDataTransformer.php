<?php

declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\ControlPointValidationInput;
use App\Entity\ControlPointValidation;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

/**
 * Data transformer of a ControlPointValidationInput into a ControlPointValidation
 */
class ControlPointValidationInputDataTransformer implements DataTransformerInterface
{
    public function __construct(private Security $security)
    {
    }

    /**
     * @param ControlPointValidationInput $object
     * @param string                      $to
     * @param array                       $context
     *
     * @return ControlPointValidation
     */
    public function transform($object, string $to, array $context = []): ControlPointValidation
    {
        if (!$user = $this->security->getUser()) {
            throw new AccessDeniedException();
        }

        $validation = new ControlPointValidation($object->controlPoint);
        $validation->setVerifier($user->getUserIdentifier());

        return $validation;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        // In case of an input, the value given here is an array (the JSON decoded).
        // If it's a ControlPointValidation, that means data ahs already been transformed
        if ($data instanceof ControlPointValidation) {
            return false;
        }

        return ControlPointValidation::class === $to
            && null !== ($context['input']['class'] ?? null)
            && 'post' === ($context['collection_operation_name'] ?? null);
    }
}
