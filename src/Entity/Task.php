<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use App\Filter\ArchivedFilter;
use App\Filter\WithinPeriodFilter;
use App\Repository\TaskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Task resource
 */
#[ORM\Entity(repositoryClass: TaskRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[ApiResource(mercure: ['private' => true, 'normalization_context' => ['groups' => 'idOnly']])]
#[ApiFilter(SearchFilter::class, properties: ['title' => 'ipartial', 'businessCase' => 'exact', 'assignedTo' => 'exact'])]
#[ApiFilter(OrderFilter::class, properties: ['createdAt'], arguments: ['orderParameterName' => 'order'])]
#[ApiFilter(DateFilter::class, properties: ['startDate'])]
#[ApiFilter(ExistsFilter::class, properties: ['startDate'])]
#[ApiFilter(WithinPeriodFilter::class)]
#[ApiFilter(ArchivedFilter::class)]
class Task
{
    use CreatedTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255)]
    #[Assert\NotBlank]
    private ?string $title = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[ORM\ManyToOne(targetEntity: BusinessCase::class, cascade: ['persist'], inversedBy: 'tasks')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank]
    private ?BusinessCase $businessCase = null;

    #[ORM\ManyToOne(targetEntity: Workforce::class)]
    private ?Workforce $assignedTo = null;

    /**
     * Low estimate duration of the task in minutes.
     * Cannot exceed 1 year.
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\Positive]
    #[Assert\LessThan(525960)]
    private ?int $durationMin = null;

    /**
     * High estimate duration of the task in minutes.
     * Cannot exceed 1 year.
     */
    #[ORM\Column(type: 'integer', nullable: true)]
    #[Assert\Positive]
    #[Assert\Expression('!value or value >= this.getDurationMin()', message: 'Max duration cannot be lower than min duration.')]
    #[Assert\LessThan(525960)]
    private ?int $durationMax = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Assert\Expression('value or !this.getEndDate()', message: 'Task cannot have end date and no start date.')]
    private ?\DateTimeInterface $startDate = null;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Assert\Expression('!value or value > this.getStartDate()', message: 'End date cannot be before start date.')]
    private ?\DateTimeImmutable $endDate = null;

    /**
     * Estimated end date based on startDate and durationMin
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    #[ApiProperty(writable: false)]
    private ?\DateTimeInterface $estimatedEndMin = null;

    /**
     * Estimated end date based on startDate and durationMax
     */
    #[ORM\Column(type: 'datetime', nullable: true)]
    #[ApiProperty(writable: false)]
    private ?\DateTimeInterface $estimatedEndMax = null;

    /**
     * @var Collection<ControlPoint>
     */
    #[ORM\OneToMany(mappedBy: 'task', targetEntity: ControlPoint::class, cascade: ['remove'], orphanRemoval: true)]
    private Collection $controlPoints;

    public function __construct()
    {
        $this->controlPoints = new ArrayCollection();
        $this->resetCreatedAt();
    }

    public function __clone()
    {
        $this->id        = null;
        $this->startDate = null;
        $this->endDate   = null;
        $controlPoints   = new ArrayCollection();
        $this->resetCreatedAt();

        // Clone each control point
        foreach ($this->controlPoints as $controlPoint) {
            $clone = clone $controlPoint;
            $clone->setTask($this);
            $controlPoints->add($clone);
        }

        $this->controlPoints = $controlPoints;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBusinessCase(): ?BusinessCase
    {
        return $this->businessCase;
    }

    public function setBusinessCase(?BusinessCase $businessCase): self
    {
        $this->businessCase = $businessCase;

        return $this;
    }

    public function getAssignedTo(): ?Workforce
    {
        return $this->assignedTo;
    }

    public function setAssignedTo(?Workforce $assignedTo): self
    {
        $this->assignedTo = $assignedTo;

        return $this;
    }

    public function getDurationMin(): ?int
    {
        return $this->durationMin;
    }

    public function setDurationMin(?int $durationMin): self
    {
        $this->durationMin = $durationMin;

        return $this;
    }

    public function getDurationMax(): ?int
    {
        return $this->durationMax;
    }

    public function setDurationMax(?int $durationMax): self
    {
        $this->durationMax = $durationMax;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeImmutable
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getEstimatedEndMin(): ?\DateTimeInterface
    {
        return $this->estimatedEndMin;
    }

    public function getEstimatedEndMax(): ?\DateTimeInterface
    {
        return $this->estimatedEndMax;
    }

    /**
     * @return Collection<ControlPoint>
     */
    public function getControlPoints(): Collection
    {
        return $this->controlPoints;
    }

    public function addControlPoint(ControlPoint $controlPoint): self
    {
        if (!$this->controlPoints->contains($controlPoint)) {
            $this->controlPoints[] = $controlPoint;
            $controlPoint->setTask($this);
        }

        return $this;
    }

    /**
     * Compute estimated end dates when persisting new task or updating existing one.
     */
    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function updateEstimatedEnds(): void
    {
        $this->estimatedEndMin = $this->getEstimatedEnd($this->durationMin);
        $this->estimatedEndMax = $this->getEstimatedEnd($this->durationMax);
    }

    /**
     * Generate end date base on $this->startDate plus given duration.
     * Return null if params are not both set.
     *
     * @param int|null $durationInMinutes
     *
     * @return \DateTimeInterface|null
     */
    private function getEstimatedEnd(?int $durationInMinutes): ?\DateTimeInterface
    {
        $enDate = null;

        if ($this->startDate && $durationInMinutes) {
            $enDate = \DateTime::createFromInterface($this->startDate)
                ->add(new \DateInterval(sprintf('PT%dM', $durationInMinutes)));
        }

        return $enDate;
    }
}
