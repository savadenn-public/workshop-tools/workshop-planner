<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait CreatedTrait
 */
trait CreatedTrait
{
    #[ORM\Column(type: "datetime_immutable")]
    private \DateTimeImmutable $createdAt;

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function __construct()
    {
        $this->resetCreatedAt();
    }

    public function resetCreatedAt()
    {
        $this->createdAt = new \DateTimeImmutable();
    }
}
