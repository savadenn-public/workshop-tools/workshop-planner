<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\ControlPointValidationInput;
use App\Repository\ControlPointValidationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Validation of a control point by a user
 */
#[ORM\Entity(repositoryClass: ControlPointValidationRepository::class)]
#[ORM\UniqueConstraint(columns: ['control_point_id', 'verifier'])]
#[ApiResource(itemOperations: [
    'get',
    'delete' => [
        'security' => 'is_granted("DELETE", object)',
    ],
], input: ControlPointValidationInput::class)]
#[ApiFilter(SearchFilter::class, properties: ['controlPoint' => 'exact'])]
#[UniqueEntity(fields: ['controlPoint', 'verifier'])]
class ControlPointValidation
{
    use CreatedTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\ManyToOne(targetEntity: ControlPoint::class, inversedBy: 'controlPointValidations')]
    #[ORM\JoinColumn(name: 'control_point_id', nullable: false)]
    private ControlPoint $controlPoint;

    /**
     * GUID of user that validated control point
     *
     * @var string
     */
    #[ORM\Column(type: 'string', length: 255)]
    private string $verifier;

    public function __construct(ControlPoint $controlPoint)
    {
        $this->controlPoint = $controlPoint;
        $this->resetCreatedAt();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getControlPoint(): ControlPoint
    {
        return $this->controlPoint;
    }

    public function setControlPoint(ControlPoint $controlPoint): self
    {
        $this->controlPoint = $controlPoint;

        return $this;
    }

    public function getVerifier(): string
    {
        return $this->verifier;
    }

    public function setVerifier(string $verifier): self
    {
        $this->verifier = $verifier;

        return $this;
    }
}
