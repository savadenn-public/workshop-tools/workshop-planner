<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PlanningSettingsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Saved planning settings
 */
#[ORM\Entity(repositoryClass: PlanningSettingsRepository::class)]
#[ApiResource(itemOperations: ['get', 'delete'])]
class PlanningSettings
{
    use CreatedTrait;

    /**
     * Granularities in hours
     */
    const GRANULARITIES = [0.25, 0.5, 1.0, 2.0, 3.0, 4.0, 6.0, 8.0, 12.0, 24.0, 48.0, 168.0];

    const DISPLAY_MODE = ['by_workforce', 'by_business_case'];

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Length(max: 255)]
    private string $name;

    /**
     * UUID of the user owner of this resource
     */
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Uuid]
    #[ApiProperty(writable: false)]
    private string $owner;

    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(4)]
    private int $blockWidth = 4;

    /**
     * Display mode
     */
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Choice(self::DISPLAY_MODE)]
    private string $mode;

    #[ORM\Column(type: 'float')]
    #[Assert\NotBlank]
    #[Assert\Choice(choices: self::GRANULARITIES)]
    private float $granularity = 6;

    /**
     * Number of days to display
     */
    #[ORM\Column(type: 'integer')]
    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(90)]
    private int $daysNumber = 7;

    /**
     * List of selected workforces
     *
     * @var Collection<Workforce>
     */
    #[ORM\ManyToMany(targetEntity: Workforce::class)]
    private Collection $selectedWorkforces;

    public function __construct()
    {
        $this->selectedWorkforces = new ArrayCollection();
        $this->resetCreatedAt();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getBlockWidth(): ?int
    {
        return $this->blockWidth;
    }

    public function setBlockWidth(int $blockWidth): self
    {
        $this->blockWidth = $blockWidth;

        return $this;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function setMode(string $mode): self
    {
        $this->mode = $mode;

        return $this;
    }

    public function getGranularity(): float
    {
        return $this->granularity;
    }

    public function setGranularity(float $granularity): self
    {
        $this->granularity = $granularity;

        return $this;
    }

    public function getDaysNumber(): int
    {
        return $this->daysNumber;
    }

    public function setDaysNumber(int $daysNumber): self
    {
        $this->daysNumber = $daysNumber;

        return $this;
    }

    /**
     * @return Collection<Workforce>
     */
    public function getSelectedWorkforces(): Collection
    {
        return $this->selectedWorkforces;
    }

    public function addSelectedWorkforce(Workforce $selectedWorkforce): self
    {
        if (!$this->selectedWorkforces->contains($selectedWorkforce)) {
            $this->selectedWorkforces[] = $selectedWorkforce;
        }

        return $this;
    }

    public function removeSelectedWorkforce(Workforce $selectedWorkforce): self
    {
        $this->selectedWorkforces->removeElement($selectedWorkforce);

        return $this;
    }
}
