<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MachineRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Machine
 */
#[ORM\Entity(repositoryClass: MachineRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
    ],
    itemOperations: [
        'get',
        'patch',
    ],
)]
class Machine extends Workforce
{
}
