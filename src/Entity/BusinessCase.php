<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\BusinessCaseStatus\Status;
use App\Controller\CloneBusinessCaseController;
use App\Repository\BusinessCaseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BusinessCase
 */
#[ORM\Entity(repositoryClass: BusinessCaseRepository::class)]
#[ApiResource(itemOperations: [
    'GET', 'PATCH', 'PUT', 'DELETE',
    'duplicate' => [
        'path'            => '/business_cases/{id}/clone',
        'method'          => 'POST',
        'controller'      => CloneBusinessCaseController::class,
        'read'            => false,
        'openapi_context' => [
            'description' => 'Make a deep copy of a business case resource',
            'summary'     => 'Make a deep copy of a business case resource',
            'requestBody' => [
                'required' => true,
                'content'  => [
                    'application/json' => [
                        'schema' => [
                            'type'       => 'object',
                            'properties' => [
                                'name' => [
                                    'type'     => 'string',
                                    'required' => true,
                                ],
                                'code' => [
                                    'type'     => 'string',
                                    'required' => true,
                                ],
                            ],
                            'example'    => [
                                'name' => 'Klugen SAS',
                                'code' => 'A564',
                            ],
                        ],
                    ],
                ],
            ],
            'responses'   => [
                201 => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/BusinessCase',
                            ],
                        ],
                    ],

                ],
            ],
        ],
    ],
], mercure: ['private' => true, 'normalization_context' => ['groups' => 'idOnly']])]
#[ApiFilter(SearchFilter::class, properties: ['code' => 'ipartial', 'name' => 'ipartial', 'status' => 'exact'])]
#[ApiFilter(DateFilter::class, properties: ['dueDate'])]
#[ApiFilter(OrderFilter::class, properties: ['dueDate' => 'DESC'], arguments: ['orderParameterName' => 'order'])]
#[UniqueEntity('code')]
class BusinessCase
{
    use CreatedTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: 'string', length: 255, unique: true, nullable: true)]
    #[Assert\Length(max: 255)]
    private ?string $code = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $dueDate = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Assert\Length(max: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'businessCase', targetEntity: Task::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $tasks;

    /**
     * Unique color for visual purpose
     */
    #[ORM\Column(type: 'string', length: 9, nullable: true)]
    #[Assert\CssColor([Assert\CssColor::HEX_LONG, Assert\CssColor::HEX_LONG_WITH_ALPHA])]
    private ?string $color = null;

    /**
     * Date the business case was archived
     */
    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $archiveDate = null;

    /**
     * Status of the business case.
     * Should not be set manually.
     */
    #[ORM\Column(type: 'string', length: 255, enumType: Status::class)]
    #[ApiProperty(writable: false)]
    private Status $status = Status::Open;

    public function __construct()
    {
        $this->resetCreatedAt();
        $this->tasks = new ArrayCollection();
    }

    public function __clone()
    {
        $this->resetCreatedAt();
        $this->id      = null;
        $this->code    = null;
        $this->dueDate = null;
        $this->color   = null;

        $tasks = new ArrayCollection();
        foreach ($this->tasks as $task) {
            $taskClone = clone $task;
            $taskClone->setBusinessCase($this);
            $tasks->add($taskClone);
        }

        $this->tasks = $tasks;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDueDate(): ?\DateTimeInterface
    {
        return $this->dueDate;
    }

    public function setDueDate(?\DateTimeInterface $dueDate): self
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getArchiveDate(): ?\DateTimeImmutable
    {
        return $this->archiveDate;
    }

    public function setArchiveDate(?\DateTimeImmutable $archiveDate): self
    {
        $this->archiveDate = $archiveDate;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(Status $status): self
    {
        $this->status = $status;

        return $this;
    }
}
